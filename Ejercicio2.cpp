#include <iostream>
using namespace std;

#include "Ejercicio2.h"
#include "Lista.h"
#include "Nombres.h"

class Ejercicio2 {
    private:

        Lista *lista = NULL;

    public:

        Ejercicio2(){
            this->lista = new Lista();
        }

        Lista *get_lista(){
            return this->lista;
        }
};

int main(){
    Ejercicio2 p = Ejercicio2();
    Lista *lista = p.get_lista();

    string nombre;

    Nombres *nombre = new Nombres();
    cout << "Nombre de la persona: ";
    cin >> nombre;
    lista->crear(nombre);

    lista->crear(new Nombres("Gerardo"));

    lista->imprimir();

    return 0;
}
