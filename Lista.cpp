#include <iostream>
using namespace std;

#include "Ejercicio2.h"
#include "Lista.h"

Lista::Lista(){
}

void Lista::crear (Nombres *nombre){
    Nodo *tmp;

    tmp = new Nodo;
    tmp->nombre = nombre;
    tmp->siguiente = NULL;
}

void Lista::imprimir(){

    Nodo *tmp = this->primero;

    while (tmp != NULL){
        cout << "Nombre: " << tmp->nombre->get_nombre() << endl;
        tmp = tmp->siguiente;
    }
}
