#include <iostream>
using namespace std;

#ifndef LISTA_H
#define LISTA_H

class Lista{
    private:
        Nodo *primero = NULL;
        Nodo *final = NULL;

    public:
        Lista();
        void crear(Nombres *nombre);
        void imprimir();
};
#endif
