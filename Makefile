prefix=/usr/local
CC = g++

CFLAGS = -g -Wall 
SRC = Ejercicio2.cpp Lista.cpp Nombres.cpp
OBJ = Ejercicio2.o Lista.o Nombres.o
APP = programa2

all: $(OBJ)
	$(CC) $(CFLAGS)-o $(APP) $(OBJ)

clean:
	$(RM) $(OBJ) $(APP)

install: $(APP)
	install -m 0755 $(APP) $(prefix)/bin

uninstall: $(APP)
	$(RM) $(prefix)/bin/$(APP)
