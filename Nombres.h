#include <iostream>
using namespace std;

#ifndef NOMBRES_H
#define NOMBRES_H

class Nombres{
    private:
        string nombre = "\0";

    public:
        Nombres(string nombre);
        string get_nombre();
};
#endif
